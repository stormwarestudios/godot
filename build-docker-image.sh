export GODOT_SEMVER="4.0"
export GODOT_RELEASE="beta6"
export GODOT_VERSION="${GODOT_SEMVER}-${GODOT_RELEASE}"
# Build environment
export IMAGE_REGISTRY="registry.gitlab.com/stormwarestudios/godot"
export IMAGE_VERSION="${GODOT_VERSION}"
export IMAGE_NAME="builder-base"
#export IMAGE_TAG="${IMAGE_REGISTRY}/${IMAGE_NAME}:${IMAGE_VERSION}"
export IMAGE_TAG="${IMAGE_NAME}:${IMAGE_VERSION}"
export DOCKERFILE="scripts/builder-base.dockerfile"
export DOCKER_BUILD_PATH="."
# Login to Docker, then build and push to the docker registry
#docker --version
#docker login -u "${CI_REGISTRY_USER}" -p "${CI_REGISTRY_PASSWORD}" "${CI_REGISTRY}"
docker build --build-arg GODOT_SEMVER --build-arg GODOT_RELEASE -f "${DOCKERFILE}" --tag "${IMAGE_TAG}" "${DOCKER_BUILD_PATH}"
#docker push "${IMAGE_TAG}"
