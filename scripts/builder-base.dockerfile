FROM ubuntu:22.04

# Environment Variables
ARG GODOT_SEMVER
ARG GODOT_RELEASE
ARG SEMANTIC_RELEASE_VERSION
ENV GODOT_PLATFORM "linux"
ENV GODOT_ARCH "x86_64"

# Install base dependencies
RUN \
    apt-get update && \
    apt-get upgrade -y && \
    apt-get install -y wget unzip libxcursor-dev libxinerama1 libxext6 libxrandr2 libxi6 libglu1-mesa-dev git curl

# Download Godot and export template, version is set from variables
RUN echo "https://downloads.tuxfamily.org/godotengine/${GODOT_SEMVER}/${GODOT_RELEASE}/Godot_v${GODOT_SEMVER}-${GODOT_RELEASE}_${GODOT_PLATFORM}.${GODOT_ARCH}.zip"
RUN \
    wget https://downloads.tuxfamily.org/godotengine/"${GODOT_SEMVER}"/${GODOT_RELEASE}/Godot_v${GODOT_SEMVER}-${GODOT_RELEASE}_${GODOT_PLATFORM}.${GODOT_ARCH}.zip && \
    wget https://downloads.tuxfamily.org/godotengine/${GODOT_SEMVER}/${GODOT_RELEASE}/Godot_v${GODOT_SEMVER}-${GODOT_RELEASE}_export_templates.tpz

# Install Godot
RUN \
    mkdir ~/.cache && \
    mkdir -p ~/.config/godot && \
    mkdir -p ~/.local/share/godot/export_templates/${GODOT_SEMVER}.${GODOT_RELEASE} && \
    unzip Godot_v${GODOT_SEMVER}-${GODOT_RELEASE}_${GODOT_PLATFORM}.${GODOT_ARCH}.zip && \
    mv Godot_v${GODOT_SEMVER}-${GODOT_RELEASE}_${GODOT_PLATFORM}.${GODOT_ARCH} /usr/local/bin/godot && \
    unzip Godot_v${GODOT_SEMVER}-${GODOT_RELEASE}_export_templates.tpz && \
    mv templates/* ~/.local/share/godot/export_templates/${GODOT_SEMVER}.${GODOT_RELEASE} && \
    rm -f Godot_v${GODOT_SEMVER}-${GODOT_RELEASE}_export_templates.tpz Godot_v${GODOT_SEMVER}-${GODOT_RELEASE}_${GODOT_PLATFORM}.${GODOT_ARCH}.zip

# Install Itch.io Butler
ADD scripts/getbutler.sh /opt/butler/getbutler.sh
RUN bash /opt/butler/getbutler.sh

# Install Node.js
## Install Node.js 18 via nodesource
RUN \
    curl -sL https://deb.nodesource.com/setup_18.x -o nodesource_setup.sh && \
    bash nodesource_setup.sh && \
    apt install nodejs && \
    npm install -g semantic-release@18 @semantic-release/gitlab @semantic-release/gitlab-config
