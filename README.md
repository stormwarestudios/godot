# Godot

This repository contains various notes and examples for the Godot game engine.

See the [Wiki](https://gitlab.com/stormwarestudios/godot/-/wikis/home) for notes.


Going for a complete breakdown of my folder structure.

I feel it's important to note that semi-serious (or more) projects should be homed in a remote version-control repository like Gitlab, Bitbucket or GitHub.

PSA over, this is a typical folder structure I use:

* `(repository root)/`
  * `Common`
    * `.gd` files (see note below about `Common`).
  * `Scripts/`
    * `.sh` build/utility/laziness scripts
  * `(project root)/`
    * `addons/`
      * Addons pulled from AssetLib.
    * `Build/`
      * Reserved for automated build systems.
    * `Common`
      * If a project requires multiple Godot binaries (i.e. client/server
        game) I'll create a symbolic link to `Common` in the repository root
        in order to share utility code between projects.
    * `Docs/`
      * various project documentation, always in `.md` format
    * `GUI/`
      * `Base/`
        * (base GUI scene name)
          * paired `.tscn` and `.gd` files
      * `Icons/`
        * `.png` / `.svg` files
      * `Scenes/`
        * `(widget name)/`
          * paired `.tscn` and `.gd` files
    * `Lib/` (scripts which don't require a related scene file; usually
      utility scripts, singletons/autoloads, abstract classes, etc)
      * `Constants.gd`
        * all the game constants; an autoload called `Constants`.
      * `GameState.gd`
        * all the game states (reusable/resettable); an autoload called
          `GameState`.
      * (class or feature name)
        * `.gd` files
    * `Media/`
      * `Audio/`
        * `.ogg` / `.wav` / `.mp3` files
      * `Fonts/`
        * `.otf` / `.ttf` files
      * `Meshes/`
        * `.gltf` / `.glb` files, related assets and `.material` files
      * `Movies/`
        * `.ogv` files
      * `Particles/`
        * `.png` files
      * `Sprites`
        * `.png` files
      * `Textures/`
        * `HDR/`
          * `.png` files
        * `Particles/`
          * `.png` files
        * `PNG/`
          * `.png` files
    * `Scenes/` (main game scenes)
      * `Main/`
        * I always set a game's entry-point to `Main.tscn` and its `Main.gd`
          counterpart.
      * `(SceneName)/`
        * Paired `SceneName.tscn` and `SceneName.gd` files, and occasionally
          immediately-related supporting files (like abstract vs concrete
          scenes/class implementations).
    * `Shaders/`
      * `.gdshader` files
    * `Testing/` (mainly used to figure things out or prototype ideas)
      * `(component name)/`
        * paired `.tscn` and `.gd` files, and any other assets required
    * `Themes/`
      * `.tres` theme files
  * `README.md`
    * Because you should love future you's sanity, and actually document things.

Various observations:
* If I happened to use a third-party media asset, I'll usually create a folder
  with its name or origin, so that I can also include license information.
* When I learned Godot, I opted for `CamelCase` folder and file names. I've
  seen arguments on both sides of the fence, but I prefer `CamelCase` to
  `snake_case`, so continue with that naming convention. (FWIW If I import
  another project's code or addons, I retain the source format.)
* I used to use a `Templates/` folder which contained abstract reusable
  scenes, but I stopped doing that in favour of storing them in `Scenes/`.
